#!/bin/bash

MOI=`cut -d: -f1 /etc/passwd | grep pingouin`
HOSTNAME

PAQUETS=""

echo
echo "#=========================="
echo "# Mise à jour et installation des logiciels"
echo "#=========================="
echo

sudo apt update
sudo apt upgrade -y

echo "# Ajout dépot openscad (non disponible ubuntu 18.04)"

sudo add-apt-repository -y ppa:openscad/releases
sudo apt update

PAQUETS="$PAQUETS openscad"

echo "#--------------------------"
echo "# Admin / dev"

echo "* APTITUDE"
PAQUETS="$PAQUETS aptitude synaptic"

echo "* ssh"
PAQUETS="$PAQUETS openssh-server"

echo "* GEANY / NEDIT / MELD"
PAQUETS="$PAQUETS geany"
PAQUETS="$PAQUETS nedit"
PAQUETS="$PAQUETS meld"

echo "* nmap"
PAQUETS="$PAQUETS nmap"

echo "* cutecom"
PAQUETS="$PAQUETS cutecom"

echo "* GIT"
PAQUETS="$PAQUETS git"

echo "#--------------------------"
echo "# Impression 3d"

echo "* SLICER"
PAQUETS="$PAQUETS slic3r"

echo "* CURA"
PAQUETS="$PAQUETS cura-engine"

echo "* PRONTERFACE"
PAQUETS="$PAQUETS printrun"

# echo "## Arduino + programmation"

# echo "* ARDUINO"
# PAQUETS="$PAQUETS arduino"
 #echo   " on lance arduino pour mettre le user dans le groupe dialout"
#arduino &

echo "* SCRATCH3"
PAQUETS="$PAQUETS scratch"

echo "* PROCESSING MANQUANT"

echo "#--------------------------"
echo "# Modélisation 3D"

echo "* FreeCAD"
PAQUETS="$PAQUETS freecad"
sudo add-apt-repository -y ppa:freecad-maintainers/freecad-stable

echo "* OPENSCAD"
PAQUETS="$PAQUETS openscad"

echo "* BLENDER"
PAQUETS="$PAQUETS blender"

echo "* meshlab"
PAQUETS="$PAQUETS meshlab"

echo "#--------------------------"
echo "# Audio"

echo "* GIMP"
PAQUETS="$PAQUETS gimp"

echo "* VLC"
PAQUETS="$PAQUETS vlc"

echo "* audacity"
PAQUETS="$PAQUETS audacity"
 
echo "#--------------------------"
echo "# Inkscape + extensions"

echo "* INKSCAPE"
PAQUETS="$PAQUETS inkscape"


echo $PAQUETS

sudo apt -y install $PAQUETS

#echo "* INKSCAPE extension laserengraver"
#cd /home/$MOI/.config/inkscape/extensions/
#if [ ! -d "/home/$MOI/.config/inkscape/extensions/laserengraver_smoothie" ]; 
#then echo "  -> installation de laserengraver "
#     git clone https://gitlab.com/PlateformeC/laserengraver_smoothie.git
#     ln -s laserengraver_smoothie/src/laserengraver_smoothie.py laserengraver_smoothie.py
#     ln -s laserengraver_smoothie/src/laserengraver_laser_smoothie.inx laserengraver_laser_smoothie.inx
#else
#    echo "  -> mise à jour de laserengraver"
#    cd laserengraver_smoothie 
#    git pull
#fi
