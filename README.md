# Script d'installation et de mise à jour des ordinateurs "PINGOUIN"

## Liste des opérations

### fichier installation-pinguoin.sh

installation des logiciels et quelques configurations particulières :
- lancement d'arduino pour dialout ?
- lancement d'inkscape et installation extension

### FondEcran.sh

- changement et personnalisation du fond d'écran

### Firefox.sh

- personnalisation de firefox : addblock + page d'accueil par défaut