#!/bin/bash

echo "* INKSCAPE extension laserengraver"
DIRLASERENGRAVER="/usr/share/inkscape/extensions/laserengraver_smoothie"

if [ ! -d $DIRLASERENGRAVER ]; 
then echo "  -> installation de laserengraver "
     cd /usr/share/inkscape/extensions
     sudo git clone https://gitlab.com/PlateformeC/laserengraver_smoothie.git
     sudo ln -s laserengraver_smoothie/src/laserengraver_smoothie.py laserengraver_smoothie.py
     sudo ln -s laserengraver_smoothie/src/laserengraver_laser_smoothie.inx laserengraver_laser_smoothie.inx
else
    echo "  -> mise à jour de laserengraver"
    cd $DIRLASERENGRAVER
    git pull
fi
