#!/bin/bash

echo "   -> Installation inkcut"

cd /home/$USER/Téléchargements

wget https://inkscape.org/gallery/item/12796/inkcut-for-inkscape-2.1.1.tar.gz

tar -xzvf inkcut-for-inkscape-2.1.1.tar.gz

# sudo apt -y install python3-pip python3-pyqt5 python3-pyqt5.qtsvg libcups2-dev
# sudo pip3 install inkcut

# git clone https://github.com/codelv/inkcut.git

sudo mkdir -p /home/pingouin/.config/inkscape/extensions
# sudo cp inkcut/plugins/inkscape/inkcut* /home/pingouin/.config/inkscape/extensions/
sudo cp /home/$USER/Téléchargements/inkscape/inkcut* /home/pingouin/.config/inkscape/extensions/
sudo chown -R pingouin:pingouin /home/pingouin/.config/inkscape

mkdir -p /home/admin_pingouin/.config/inkscape/extensions
sudo cp /home/$USER/Téléchargements/inkscape/inkcut* /home/admin_pingouin/.config/inkscape/extensions/

rm -r /home/$USER/Téléchargements/inkcut-for-inkscape-2.1.1.tar.gz
rm -r /home/$USER/Téléchargements/inkscape/